package org.eclipse.smarthome.binding.mainzigartig.internal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 *
 * @author Daniel Köler
 */
public class DocumentParser {

    private final List<MappedElement> lstElements;

    public DocumentParser() {
        lstElements = new ArrayList<>();
    }

    public void parseDocument(Document docToParse) {
        Elements elLines = docToParse.getElementsByClass("itcs-data_table-dataLinie");
        Elements elRichtung = docToParse.getElementsByClass("itcs-data_table-dataZiel");
        Elements elAbfahrt = docToParse.getElementsByClass("itcs-data_table-dataZeit");

        for (int i = 0; i < elLines.size(); i++) {
            MappedElement elMapped = new MappedElement();
            elMapped.setStrRichtung(elRichtung.get(i).text());
            elMapped.setStrLine(elLines.get(i).text());
            elMapped.setStrTime(elAbfahrt.get(i).text());

            lstElements.add(elMapped);
        }
    }

    public List<MappedElement> getLstElements() {
        return lstElements;
    }

    public String getStringElements(Document doc) {
        parseDocument(doc);
        StringBuilder builder = new StringBuilder();
        for (MappedElement el : lstElements) {
            builder.append("#");
            builder.append(el.getStrLine());
            builder.append("?");
            builder.append(el.getStrRichtung());
            builder.append("?");
            builder.append(el.getStrTime());
        }
        return builder.toString();
    }
}
