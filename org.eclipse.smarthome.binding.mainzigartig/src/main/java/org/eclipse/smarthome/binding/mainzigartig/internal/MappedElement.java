package org.eclipse.smarthome.binding.mainzigartig.internal;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Daniel Köhler
 */
public class MappedElement {

    private String strLine;
    private String strTime;
    private String strRichtung;

    public String getStrLine() {
        return strLine;
    }

    public void setStrLine(String strStation) {
        this.strLine = strStation;
    }

    public String getStrTime() {
        return strTime;
    }

    public void setStrTime(String strTime) {
        this.strTime = strTime;
    }

    public String getStrRichtung() {
        return strRichtung;
    }

    public void setStrRichtung(String strRichtung) {
        this.strRichtung = strRichtung;
    }

}
