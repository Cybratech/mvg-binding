/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.eclipse.smarthome.binding.mainzigartig.internal;

import java.io.IOException;
import java.net.MalformedURLException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 *
 * @author Daniel Köhler
 */
public class Requester {

    private final String URL = "http://mobil.mvg-mainz.de/typo3conf/ext/"
            + "itcsoutput/Classes/Utility/autocomplete__1.php?p1=427&q=";
    private final String fragment1 = "&p3=";
    private final String fragment2 = "&p12=";
    private final String myRv = "230043422";
    private final String station = "Hildegard-v.-Bingen-Str."; // TODO: PARAMETERIZE
    private final String time = "30";

    public Requester() {

    }

    public Document buildRequest() throws MalformedURLException, IOException {
        String uriRequest = URL + EncodingUtil.decodeURIComponent(station) + fragment1 + time + fragment2 + myRv;
        Document doc = Jsoup.connect(uriRequest).get();
        return doc;
    }
}
