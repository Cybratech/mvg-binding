/**
 * Copyright (c) 2014,2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.smarthome.binding.mainzigartig.internal;

import java.util.Collections;
import java.util.Set;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.smarthome.core.thing.ThingTypeUID;

/**
 * The {@link mainzigartigBindingConstants} class defines common constants, which are
 * used across the whole binding.
 *
 * @author Daniel Koehler - Initial contribution
 */
@NonNullByDefault
public class mainzigartigBindingConstants {

    private static final String BINDING_ID = "mainzigartig";

    // List of all Thing Type UIDs
    public static final ThingTypeUID THING_TYPE_MVG = new ThingTypeUID(BINDING_ID, "mvg");

    // List of all Channel id's
    public static final String CHANNEL_ABFAHRT = "abfahrt";

    public static final Set<ThingTypeUID> SUPPORTED_THING_TYPES_UIDS = Collections.singleton(THING_TYPE_MVG);
}
