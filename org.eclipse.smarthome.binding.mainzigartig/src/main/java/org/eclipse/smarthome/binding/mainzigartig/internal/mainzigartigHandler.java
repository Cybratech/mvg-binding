/**
 * Copyright (c) 2014,2018 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0
 *
 * SPDX-License-Identifier: EPL-2.0
 */
package org.eclipse.smarthome.binding.mainzigartig.internal;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.eclipse.smarthome.config.core.Configuration;
import org.eclipse.smarthome.core.cache.ExpiringCacheMap;
import org.eclipse.smarthome.core.library.types.StringType;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.ThingStatusDetail;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.RefreshType;
import org.eclipse.smarthome.core.types.State;
import org.eclipse.smarthome.core.types.UnDefType;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link mainzigartigHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author Daniel Koehler - Initial contribution
 */
public class mainzigartigHandler extends BaseThingHandler {

    private final Logger logger = LoggerFactory.getLogger(mainzigartigHandler.class);
    private static final String LOCATION_PARAM = "HVB";

    private long lastUpdateTime;
    private String location = "";
    private BigDecimal refresh = new BigDecimal(60);
    private String abfahrtData = null;

    private static final int MAX_DATA_AGE = 100 * 1000; // 100s
    private static final int CACHE_EXPIRY = 10 * 1000; // 10s
    private static final String CACHE_KEY_CONFIG = "CONFIG_STATUS";
    private static final String CACHE_KEY_ABFAHRT = "ABFAHRT";
    private Requester req;
    private DocumentParser docParser;

    private final ExpiringCacheMap<String, String> cache = new ExpiringCacheMap<>(CACHE_EXPIRY);

    ScheduledFuture<?> refreshJob;

    public mainzigartigHandler(Thing thing) {
        super(thing);
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        if (command instanceof RefreshType) {
            boolean success = updateAbfahrtData();
            if (success) {
                switch (channelUID.getId()) {
                    case mainzigartigBindingConstants.CHANNEL_ABFAHRT:
                        updateState(channelUID, getAbfahrten());
                        break;
                    default:
                        logger.debug("Command received for an unknown channel: {}", channelUID.getId());
                        break;
                }
            }
        } else {
            logger.debug("Command {} is not supported for channel: {}", command, channelUID.getId());
        }
    }

    @Override
    public void initialize() {
        logger.debug("Start initialising!");
        Configuration config = getThing().getConfiguration();
        location = (String) config.get(LOCATION_PARAM);
        try {
            refresh = (BigDecimal) config.get("refresh");
        } catch (Exception e) {
            logger.debug("Cannot set refresh parameter.", e);
        }

        if (refresh == null) {
            // let's go for the default
            refresh = new BigDecimal(60);
        }
        req = new Requester();
        try {
            Document doc = req.buildRequest();
            docParser = new DocumentParser();
            cache.put(CACHE_KEY_ABFAHRT, () -> docParser.getStringElements(doc));
        } catch (IOException e) {
            logger.error("Cant establish connection to MVG Website");
        }

        // TODO: ADD Logics with Request
        /*
         * cache.put(CACHE_KEY_CONFIG, () -> connection.getResponseFromQuery(
         * "SELECT location FROM weather.forecast WHERE woeid = " + location.toPlainString()));
         * cache.put(CACHE_KEY_WEATHER, () -> connection.getResponseFromQuery(
         * "SELECT * FROM weather.forecast WHERE u = 'c' AND woeid = " + location.toPlainString()));
         */
        startAutomaticRefresh();
    }

    private void startAutomaticRefresh() {
        refreshJob = scheduler.scheduleWithFixedDelay(() -> {
            try {
                boolean success = updateAbfahrtData();
                if (success) {
                    updateState(new ChannelUID(getThing().getUID(), mainzigartigBindingConstants.CHANNEL_ABFAHRT),
                            getAbfahrten());
                }
            } catch (Exception e) {
                logger.debug("Exception occurred during execution: {}", e.getMessage(), e);
                updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.OFFLINE.COMMUNICATION_ERROR, e.getMessage());
            }
        }, 0, refresh.intValue(), TimeUnit.SECONDS);

    }

    @Override
    public void dispose() {
        refreshJob.cancel(true);
    }

    private synchronized boolean updateAbfahrtData() {
        final String data = cache.get(CACHE_KEY_ABFAHRT);
        if (data != null) {
            if (data.isEmpty()) {
                if (isCurrentDataExpired()) {
                    logger.trace("The API did not return any data. Omiting the old result because it became too old.");
                    abfahrtData = null;
                    updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.OFFLINE.COMMUNICATION_ERROR,
                            "@text/offline.no-data");
                    return false;
                } else {
                    // simply keep the old data
                    logger.trace("The API did not return any data. Keeping the old result.");
                    return false;
                }
            } else {
                lastUpdateTime = System.currentTimeMillis();
                abfahrtData = data;
            }
            updateStatus(ThingStatus.ONLINE);
            return true;
        }
        abfahrtData = null;
        updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.OFFLINE.COMMUNICATION_ERROR,
                "@text/offline.location [\"" + "\"");
        return false;
    }

    private State getAbfahrten() {
        if (abfahrtData != null) {
            return new StringType(abfahrtData);
        }
        return UnDefType.UNDEF;
    }

    private boolean isCurrentDataExpired() {
        return lastUpdateTime + MAX_DATA_AGE < System.currentTimeMillis();
    }
}
